#!/bin/bash
cat <<CRON |
`
crontab -l | sed '/#wxu_cron_begin/,/#wxu_cron_end/d';
php -r '
   $arr = [
	[
              "day" => 26,
	      "mouth" => 3,
	      "hour" => 3,
	      "min" => 26,
              "week" => "*",
	      "commond" => "ls -al"
	],
	[
              "day" => 26,
              "mouth" => 3,
              "hour" => 3,
              "min" => 26,
              "week" => "*",
              "commond" => "ls ~/"
        ],
   ];
   echo "#wxu_cron_begin" . "\n";
   array_walk($arr, function($val, $key) {
        echo "{$val["min"]} {$val["hour"]} {$val["day"]} {$val["mouth"]} {$val["week"]} {$val["commond"]}" . "\n";
   });
   echo "#wxu_cron_end" . "\n";
'
`
CRON
cat | crontab - 
