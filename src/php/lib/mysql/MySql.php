<?php

namespace tool\mysql;

use \PDO;
use \PDOException;

class MySQL {
    public static function init($siteConf, $readOnly = true) {
        for ($i = 0; $i < 3; $i++) {
            $rs = self::_init($siteConf, $readOnly);
            if ($rs != false) {
                return $rs;
            } else {
                sleep($i * 2);
            }
        }
    }

    private static function _init($siteConf, $readOnly) {
        $hostKey = $readOnly ? 'db_host' : 'dbw_host';
        $portKey = $readOnly ? 'db_port' : 'dbw_port';
        $port = empty($siteConf[$portKey]) ? 3306 : $siteConf[$portKey];
        try {
            if (defined('\PDO::MYSQL_ATTR_INIT_COMMAND')) {
                $dbh = new PDO("mysql:host={$siteConf[$hostKey]};port={$port};charset=utf8;dbname={$siteConf['db_name']}",
                    $siteConf['db_user'], $siteConf['db_pass'],
                    array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8;'));
            } else {
                $dbh = new PDO("mysql:host={$siteConf[$hostKey]};port={$port};charset=utf8;dbname={$siteConf['db_name']}",
                    $siteConf['db_user'], $siteConf['db_pass']);
            }
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $initCmds = array(
                "SET sql_mode='';",
            );
            if (isset($siteConf['db_wait_timeout'])) {
                $wait_timeout = intval($siteConf['db_wait_timeout']);
                if ($wait_timeout > 0) {
                    $initCmds[] = "SET wait_timeout = {$wait_timeout};";
                }
            }
            if (isset($siteConf['db_timezone'])){
                 $db_timezone = $siteConf['db_timezone'];
                 if(!empty($db_timezone)) {
                     $initCmds[] = "SET time_zone = '{$db_timezone}';";
                 }
            }

            foreach($initCmds as $cmd){
                $dbh->exec($cmd);
            }
            return $dbh;
        }
        catch (PDOException $e) {
            print_r($e);
            die;
        }
    }
}
