<?php
namespace tool\mysql\base;

abstract class BaseDao {
    public $container;
    private $dbPrefix;

    public function __construct($container, $dbPrefix = "") {
        $this->container = $container;
        $this->dbPrefix = $dbPrefix;
    }

    protected function _T($table) {
        return $table;
    }

    protected function db(){
        return $this->container[$this->dbPrefix . 'db'];
    }

    protected function dbw(){
        return $this->container[$this->dbPrefix . 'dbw'];
    }


    public function exec($sql, $params = array()) {
        try {
            $pstmt = $this->db()->prepare($sql);
            return $pstmt->execute($params);
        }
        catch(\PDOException $e) {
            print_r($e);
            die;
        }
        return false;
    }

    public function getMtArr($sql, $params = array()) {
        try {
            $pstmt = $this->db()->prepare($sql);
            if($pstmt->execute($params)) {
                 $rs = $pstmt->fetchAll(\PDO::FETCH_ASSOC);
                 return $rs;
            }
        }
        catch(\PDOException $e) {
            print_r($e);
            die;
        }
        return array();
    }

    public function getArr($sql, $params = array()) {
        try {
            $pstmt = $this->db()->prepare($sql);
            if($pstmt->execute($params)) {
                 $rs = $pstmt->fetch(\PDO::FETCH_ASSOC);
                 return $rs;
            }
        }
        catch(\PDOException $e) {
            print_r($e);
            die;
        }
        return array();
    }


}
