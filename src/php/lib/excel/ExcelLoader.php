<?php
namespace tool\excel;

class ExcelLoader {

    public function loadMtArr($file, $sheetIndex = 0) {
        $objPhpExcel = \PHPExcel_IOFactory::load($file);
        $sheets = $objPhpExcel->getAllSheets();
        if (count($sheets) <= $sheetIndex) {
            return [];
        }
        $sheet = $sheets[$sheetIndex];
        //col start 0
        $maxCol = \PHPExcel_Cell::columnIndexFromString($sheet->getHighestColumn());
        //row start 1
        $maxRow = $sheet->getHighestRow();
        $title = $this->getTitle($sheet, $maxCol);
        $mtArr = $this->getBody($sheet, $title, $maxRow);
        return $mtArr;
    }
    private function getTitle($sheet, $maxCol) {
        $arr = array();
        for ($i = 0; $i < $maxCol; $i ++) {
            $cell = $sheet->getCellByColumnAndRow($i, 1, false);
            if(!$cell) {
                continue;
            }
            $val = $cell->getFormattedValue();
            if (!empty($val)) {
                array_push($arr, $val);
            }
        }
        return $arr;
    }
    private function getBody($sheet, $title, $maxRow) {
        //start from 2
        $realCol = count($title);
        $mtArr = array();
        for ($row = 2; $row <= $maxRow; $row++ ) {
            $colArr = array();
            for($col = 0; $col < $realCol; $col++){
                $cell = $sheet->getCellByColumnAndRow($col, $row, false);
                if(!$cell) {
                    continue;
                }
                $val = $cell->getFormattedValue();
                if (!empty($val)) {
                    $colArr[$title[$col]] = $val;
                }
            }
            array_push($mtArr, $colArr);
        }
        return $mtArr;
    }
}