<?php

namespace tool\excel;


class GenerateExcel {



    public function data2xlsx($arrs, $file) {
        $objPHPExcel = new \PHPExcel();
        //一些不明白的初始化
        $objPHPExcel->getProperties()
            ->setCreator("wxu")
            ->setTitle("data report")
            ->setSubject("PHPExcel")
            ->setKeywords("office PHPExcel php")
            ->setCategory("default");
        $this->setDataTitle($objPHPExcel, $arrs);
        $this->setDataBody($objPHPExcel, $arrs);
       $objWriter = \PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
       $objWriter->save(str_replace('.php', '.xlsx', $file));
    }

    private function setDataTitle($excelHandler, $arrs) {
        $titles = $this->getMtArrKeys($arrs);
        $colIndex = 1;
        foreach ($titles as $val) {
            $excelHandler->setActiveSheetIndex(0)
            ->setCellValue(
                $this->decimal2ABC($colIndex) . "1",
                $val
            );
            $colIndex++;
        }
    }

    private function setDataBody($excelHandler, $arrs) {
        $titles = $this->getMtArrKeys($arrs);
        //data body start with 2
        $rowIndex = 2;
        foreach ($arrs as $arr) {
            $colIndex = 1;
            foreach ($arr as $key => $val) {
                $excelHandler->setActiveSheetIndex(0)
                ->setCellValue(
                    $this->decimal2ABC($colIndex) . $rowIndex,
                    $val 
                );
                $colIndex++;
            }
            $rowIndex++;
        }
    }


    private function decimal2ABC($pos) {
        if($pos > 0 && $pos <= 26) {
            //just simple
            return strval(chr(65 + $pos - 1));
        }
    }

    //获取二维数组中, 最长数组的键名
    private function getMtArrKeys($arrs) {
        $tempArr = array();
        foreach ($arrs as $arr) {
            if (count($arr) > count($tempArr)) {
                $tempArr = $arr;
            }
        }
        return array_keys($tempArr);
    }



}