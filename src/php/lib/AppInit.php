<?php

namespace tool;


use tool\mysql\MySQL;
use tool\mysql\base\MetaDao;

class AppInit {
    public function initConf($container) {
        $container['siteConf'] = function($c) {
            include $c['APP_ROOT'].'/etc/env_config.php';
	    if(isset($c['stage']) && !empty($c['stage'])) {
	        $siteConf['stage'] = $c['stage'];
	    }
            if(isset($siteConf['stage'])){
                $stage = strtolower($siteConf['stage']);
                $stage_conf = $c['APP_ROOT']."/etc/stage.$stage.php";
                if (is_readable($stage_conf)) {
                    include $stage_conf;
                }
            }
            return isset($siteConf) ? $siteConf : array();
        };
        return $container;
    }


    public function initBase($container) {
        $container['db'] = function($c){
            $siteConf = $c['siteConf'];
            return MySQL::init($siteConf, true);
        };
        $container['dao'] = function($c) {
            return new MetaDao($c);
        };
        return $container;
    }
}
