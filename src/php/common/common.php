<?php

$APP_ROOT = realpath(__DIR__ . '/../../../')  . '/';

require_once $APP_ROOT . "vendor/autoload.php";
$opts = getopt('s:');
$container = new Pimple\Container();

$container['APP_ROOT'] = $APP_ROOT;
$container['stage'] = isset($opts['s']) ? $opts['s'] : '';

$appInit= new tool\AppInit();

$container = $appInit->initConf($container);
$container = $appInit->initBase($container);
