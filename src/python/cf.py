#!/usr/bin/python

import sys
import os
import argparse
import CloudFlare
import json
import subprocess
from argparse import RawTextHelpFormatter

cf = CloudFlare.CloudFlare()

cli = argparse.ArgumentParser(formatter_class=RawTextHelpFormatter, description="""
    This is for cloudflare command line
    --- cf.py -z example.com delete -n test
    --- cf.py -z example.com add -n test -t A -c 127.0.0.1
    --- cf.py -z example.com list
    --- cf.py -z example.com clean -a true
""")

cli.add_argument('-z','--zone', help='dns zone',required=True)

subparsers = cli.add_subparsers(dest="subcommand")

def subcommand(args=[], parent=subparsers):
    def decorator(func):
        parser = parent.add_parser(func.__name__, description=func.__doc__)
        for arg in args:
            parser.add_argument(*arg[0], **arg[1])
        parser.set_defaults(func=func)
    return decorator

def argument(*name_or_flags, **kwargs):
    return (name_or_flags, kwargs)

def render(data, isJson=True):
    if isJson:
        print json.dumps(data, indent=4, sort_keys=True)
    else:
        print data

def getZoneId(args):
    try:
        zones = cf.zones.get(params={'name': args.zone})
    except:
        exit("Get Zone ID:",sys.exc_info())
    return zones[0]['id']

@subcommand([])
def list(args):
    try:
        zondId = getZoneId(args)
        dnsRecords = cf.zones.dns_records.get(zondId)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print e
        exit()
    render(dnsRecords)

@subcommand([
    argument('-n','--name', help='DNS Name',required=True),
    argument('-t','--type', help='DNS Type',required=True),
    argument('-c','--content', help='DNS Address',required=True),
    argument('-p', '--priority', nargs='?', help='mx record priority')
])
def add(args):
    dnsName = args.zone if args.name == '@' else args.name + '.' + args.zone
    dnsRecord = {
        'name': dnsName,
        'type': args.type,
        'content': args.content,
        'proxied': True
    }
    if args.priority is not None and args.type == 'MX':
        dnsRecord['priority'] = int(args.priority)
        dnsRecord['proxied'] = False

    try:
        zondId = getZoneId(args)
        rs = cf.zones.dns_records.post(zondId, data=json.dumps(dnsRecord))
        render(rs)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print e
        exit()
@subcommand([
    argument('-n','--name', help='DNS Name',required=True),
    argument('-t', '--type', nargs='?', help='update dns type'),
    argument('-c', '--content', nargs='?', help='dns address'),
    argument('-p', '--proxied', nargs='?', help='enable ssl ca')
])
def update(args):
    zondId = getZoneId(args)
    dnsName = args.zone if args.name == '@' else args.name + '.' + args.zone
    dnsRecords = cf.zones.dns_records.get(zondId, params={'name':dnsName})
    for dnsRecord in dnsRecords:
        updateData = { 'name': dnsName}
        updateData['type'] = args.type if args.type is not None else dnsRecord['type']
        updateData['content'] = args.content if args.content is not None else dnsRecord['content']
        # don't update mx record
        if updateData['type'] == 'MX':
            continue
        updateData['proxied'] = (args.proxied.lower() in ("yes", "true", "1")) if args.proxied is not None else dnsRecord['proxied']
        rs = cf.zones.dns_records.put(zondId, dnsRecord['id'], data=json.dumps(updateData))
        render(rs)

@subcommand([
    argument('-n','--name', help='DNS Name',required=True),
])
def delete(args):
    try:
        zondId = getZoneId(args)
        dnsName = args.zone if args.name == '@' else args.name + '.' + args.zone
        dnsRecords = cf.zones.dns_records.get(zondId, params={'name':dnsName})
        for dnsRecord in dnsRecords:
            rs = cf.zones.dns_records.delete(zondId, dnsRecord['id'])
            render(rs)
    except CloudFlare.exceptions.CloudFlareAPIError as e:
        print e
        print sys.exc_info()
        exit()

@subcommand([
    argument('-a', '--all', nargs='?', help='clean all cache'),
    argument('-p','--path', help='clean specify path cache')
])
def clean(args):
    zondId = getZoneId(args)
    #cf.zones.purge_cache.delete(zondId, data=json.dumps(dnsRecord))
    cleanAll = bool(args.all)
    cleanPath = args.path
    rs = {}
    if cleanAll:
        rs = cf.zones.purge_cache.delete(zondId, data=json.dumps({
            'purge_everything': cleanAll
        }))
    if cleanPath is not None:
        files = cleanPath.split(',')
        data = {'files': files}
        rs = cf.zones.purge_cache.delete(zondId, data=json.dumps(data))
    render(rs)

@subcommand([
    argument('-f','--froms', help='match url, now only support forwarding url',required=True),
    argument('-t', '--to',  help='forwarding_url', required=True),
    argument('-c', '--code', nargs='?', help='forwarding url code, 301, 302'),
])
def page(args):
    zone = args.zone
    code = args.code if args.code is not None else 302
    fromUrl = args.froms
    toUrl = args.to
    targets = [{
        "target": "url",
        "constraint": {
            "operator": "matches",
            "value": fromUrl
        }
    }]
    actions = [
        {
            "id": "forwarding_url",
            "value": {
                "status_code": int(code),
                "url": toUrl
            }
        }
    ]

    cmd = [ "cli4", "--post",  "targets=" + json.dumps(targets), "actions=" + json.dumps(actions) ]
    cmd.extend(["status=active", "priority=1", "/zones/:" + zone + "/pagerules"])
    rs = subprocess.check_output(cmd)
    render(rs, False)

args = cli.parse_args()
if args.subcommand is None:
    cli.print_help()
else:
    args.func(args)
