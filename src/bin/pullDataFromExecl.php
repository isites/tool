<?php
require_once __DIR__ . "/../php/common/common.php";

use tool\excel\GenerateExcel;
use tool\excel\ExcelLoader;

function getSalesData($start_time, $end_time, $pc_domain, $m_domain, $code) {
    global $container;
    // SET @ac_start_time := '$start_time';
    // SET @ac_end_time   := '$end_time';
    // SET @pc_domain :='$pc_domain';
    // SET @m_domain :='$m_domain';
    // SET @coupon_code   := 'BUY10';
    $sql = <<<SQL

    SELECT 
        region.region_name '国家',
        DATE_FORMAT(oi.order_time, "%Y-%m-%d") AS date,  -- 美国时间
        count(*) '订单数', 
        SUM(oi.goods_amount) '商品金额', 
        SUM(IF(oi.coupon_code='$code', 1, 0)) '{$code}使用数量',
        SUM(IF(oi.coupon_code='$code', oi.bonus*(-1), 0)) '{$code}使用金额',
        -- SUM(IF(oi.coupon_code!='', 1, 0)) '所有红包数量(所有类型红包)',
        SUM(IF(oi.coupon_code!='', oi.bonus*(-1), 0)) '所有红包金额(所有类型红包)'
        -- oi.from_domain 'site'
        -- IF(oi.from_domain REGEXP 'www.jjshouse', 'PC', "Mobile") 'PC/Mobile'
    FROM order_info oi
    JOIN region ON oi.country = region.region_id
    WHERE 
        oi.order_time > '$start_time'
    AND oi.order_time < '$end_time'
    AND oi.email NOT REGEXP '(tetx|jjshouse|i9i8|qq|163)\.com'
    AND oi.from_domain IN (
        '$pc_domain', '$m_domain'
    )
    AND oi.pay_status > 0
    GROUP BY region_id, date
    ORDER BY region.region_name, date ASC;
SQL;
    echo $sql . "\n";
    return $container['dao']->getMtArr($sql);
}

$siteMap = array(
    'js' => ['www.jjshouse.com', 'm.jjshouse.com'],
    'jsuk' => ['www.jjshouse.co.uk', 'm.jjshouse.co.uk'],
    'jsfr' => ['www.jjshouse.fr', 'm.jjshouse.fr'],
    'jsde' => ['www.jjshouse.de', 'm.jjshouse.de'],
    'jsno' => ['www.jjshouse.no', 'm.jjshouse.no'],
    'jsse' => ['www.jjshouse.se', 'm.jjshouse.se'],
    'jsch' => ['www.jjshouse.ch', 'm.jjshouse.ch'],
    'jsau' => ['www.jjshouse.com.au', 'm.jjshouse.com.au'],
);

$opts = getopt('s:f:i:');
isset($opts['f']) || die;
$file = $opts['f'];
$index = isset($opts['i']) ? $opts['i'] : 0;
$loader = new ExcelLoader();
$dataSource = $loader->loadMtArr($file, $index);
$excelHandler = new GenerateExcel();
foreach($dataSource as $source) {
    $sites = explode(",",$source['site']);
    $times = explode(",",$source['time']);
    $coupon = trim($source['coupon']);
    foreach($sites as $site) {
        $site = trim($site);
        $domains = $siteMap[$site];
        $datas = array();
        foreach($times as $time) {
            $arr = explode("~", $time);
            $from_time = trim($arr[0]);
            $end_time = trim($arr[1]);
            $datas = array_merge($datas, 
                getSalesData($from_time, $end_time, $domains[0], $domains[1], $coupon)
            );
        }
        $excelHandler->data2xlsx($datas, "/vagrant/$site.xlsx");
    }
    
}

