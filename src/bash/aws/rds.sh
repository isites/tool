
rdsStatus() {
    profile=$1
    myexec aws rds --profile $profile describe-db-instances | jq ".DBInstances[]" | jq ".DBInstanceArn" | xargs -I {} myexec aws rds --profile $profile describe-pending-maintenance-actions --resource-identifier {}
}
