

cflist() {
    name=$1
    profile=${1:-js}
    myexec aws cloudfront --profile $profile list-distributions   
}

cfinvalidation() {
    distributionId=$1
    profile=$2
    check=$3
    if [  $# -eq 0 ]; then
        cat <<USAGE
    Usage: $0 [distributionId] [profile] [check]
        eg: cfinvalidation E2I5TRW7QTCTCP jt true
        eg: cfinvalidation E2I5TRW7QTCTCP jt false
USAGE
        return
    fi;
    shift 3
    res=$(myexec aws cloudfront --profile $profile create-invalidation --distribution-id $distributionId --paths $@)
    if [ "$check" = "true" ]; then
        invalidationId=$(echo $res | jq -r '.Invalidation.Id' 2> /dev/null)
        invalidationStatus=$(echo $res | jq -r '.Invalidation.Status' 2> /dev/null)
        invalidationprocess=
        while [ "$invalidationStatus" != "Completed" ]
        do
            clear_output "Cehck invalidation."
            sleep 0.5
            clear_output "Cehck invalidation.."
            sleep 0.5
            clear_output "Cehck invalidation..."
            invalidationprocess=$(cfinvalidation_check $distributionId $invalidationId $profile)
            invalidationStatus=$(echo $invalidationprocess | jq -r '.Invalidation.Status' 2> /dev/null)
        done
        echo
        echo $invalidationprocess
    else
        echo $res
    fi;
}

cfinvalidation_check() {
    distributionId=$1
    invalidationId=$2
    profile=$3
    myexec aws cloudfront --profile $profile get-invalidation --id $invalidationId --distribution-id $distributionId
}

clear_output() {
    printf '\r%s' "$*"
    #printf "\033[A"
}