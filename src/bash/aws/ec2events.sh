#!/bin/bash

profile=${1:-jt}

source /vagrant/tool/src/bash/aws/ec2.sh

ec2desc "" $profile | jq -r ".Reservations[] | .Instances[] | .InstanceId" | while read row;
do
   json=$(ec2descById $row $profile)
   name=$(echo $json | jq ".Reservations[]" 2> /dev/null | jq ".Instances[]" 2> /dev/null | jq -r '.Tags[]|select(.Key=="Name") | .Value')
   state=$(echo $json | jq -r ".Reservations[] | .Instances[] | .State.Name")
   ip=$(echo $json |  jq -r ".Reservations[] | .Instances[] | .NetworkInterfaces[] | .Association | .PublicIp" 2> /dev/null)
   if [ -z "$ip" ];then
     ip=$(echo $json | jq -r ".Reservations[] | .Instances[] | .PublicIpAddress" 2> /dev/null)
   fi;
   eip=
   if [ -n "$ip" ] && [ "$ip" != "null" ]; then
     _id=$(ec2ipdesc $ip $profile 2> /dev/null | jq -r ".Addresses[] | .InstanceId" 2>/dev/null)
     if [ "$row" = "$_id" ]; then
         eip=$ip
     fi;
   fi
   printf "${name}\t${state}-${eip}\t${row}\t"
   res=$(ec2status $profile $row | jq ".InstanceStatuses[]" 2> /dev/null | jq ".Events[].Description" 2> /dev/null)
   echo $res
done
