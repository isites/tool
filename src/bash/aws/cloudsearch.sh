#!/bin/bash


mycloudSearch() {
    catId=$1
    profile=${2:-jt}
    index_version=${3:-7}
    attr=$4
    type=${5:-product}
    start=${6:-0}
    size=${7:-48}
    order=${8:-sales}

    if [  $# -eq 0 ]; then
        cat <<USAGE
    Usage: $0 [catId] [profile] [index_version] [attr] [type] [start] [size] [order: sales,new]
        eg: mycloudSearch 47 hd 7 54198:54867 product 0 1
        eg: mycloudSearch 47 hd 7
USAGE
        return
    fi;

    if [ "$profile" = "jt" ]; then
        stage=dev
    elif [ "$profile" = "hd" ]; then 
        stage=as
    else 
        stage=$profile
    fi;
    source $BASE_DIR/etc/bash/$stage
    cmd='myexec aws cloudsearchdomain search --query-parser structured --profile $profile --endpoint-url $search_url --search-query '
    condition="(and (term field=lists 'c${catId}_top_sellers_sales') (term field=type '$type') (term field=version '$index_version')"
    if [ -n "$attr" ]; then 
        condition=$condition"  (or (term field=attributes '$attr'))"
    fi;
    condition=$condition")"
    if [ "$order" = "new" ]; then
        cmd=$cmd' "$condition" '' --sort="fresh_time desc" --start $start --size $size'
    elif [ "$order" = "sales" ];then
        cmd=$cmd' "$condition" '' --sort="c${catId}_top_sellers_sales_order asc" --start $start --size $size'
    fi
    eval $cmd
}

