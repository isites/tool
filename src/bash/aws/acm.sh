acmlist() {
    profile=${1:-jt}
    myexec aws acm --profile $profile list-certificates
}

acmdesc() {
    arn=$1
    profile=${2:-jt}
    myexec aws acm --profile $profile describe-certificate --certificate-arn $2
}

acmdel() {
    arn=$1
    profile=${2:-jt}
    myexec aws acm --profile $profile delete-certificate --certificate-arn $2
}