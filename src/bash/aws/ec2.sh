#!/bin/bash
#aws route53 change-resource-record-sets --profile je --hosted-zone-id  $zone_id --change-batch file:///vagrant/route53.json
ec2desc() {
    stage=$1
    profile=${2:-jt}
    myexec aws ec2 --profile $profile describe-instances --filters "Name=instance-state-name,Values=running" --filters "Name=tag:aws:cloudformation:stack-name,Values=lestore-stage-$stage*"
}

ec2descById() {
   id=$1
   profile=${2:-jt}
   myexec aws ec2 --profile $profile describe-instances --instance-ids $id
}

ec2status() {
    profile=$1
    shift 1
    myexec aws ec2 --profile $profile describe-instance-status --instance-ids $@
}

ec2stop() {
    profile=$1
    shift 1
    myexec aws ec2 --profile $profile stop-instances --instance-ids $@
}

ec2start() {
   profile=$1
   shift 1
   myexec aws ec2 --profile $profile start-instances --instance-ids $@    
}


ec2ipdesc() {
   ip=$1
   profile=${2:-jt}
   myexec aws ec2 --profile $profile describe-addresses --public-ips $ip
}

ec2GetIp() {
   profile=$1
   region=$2
   cmd=
   if [ -n "$region" ];then
      cmd="--region $region"
   fi;
   myexec aws ec2 allocate-address --profile $profile $cmd
}

ec2AssociateIp() {
  if [  $# -eq 0 ]; then
        cat <<USAGE
  Usage: $0 [instance id] [ip] [profile] [region]
USAGE
     return 
  fi;
  instanceId=$1
  ip=$2
  profile=${3:-jt}
  region=$4
  cmd=
  if [ -n "$region" ]; then
     cmd="--region $region"
  fi;
  myexec aws ec2 associate-address --profile $profile $cmd --instance-id $instanceId --public-ip $ip
}
ec2SecurityQuery() {
  query=$1
  profile=${2:-jt}
  region=$3
  cmd=
  if [ -n "$region" ]; then
     cmd="--region $region"
  fi;
  #myexec aws ec2 describe-security-groups --profile $profile $cmd | jq '.SecurityGroups[] | .IpPermissions[] | "\(.ToPort)" + "-" + "\(.FromPort)" + ":" +(.IpRanges[] | select(.CidrIp=="$query") | .CidrIp)'
  execCmd='myexec aws ec2 describe-security-groups --group-ids $groupId --profile $profile $cmd'
  execCmd=$execCmd" | jq -r '.SecurityGroups[] | .IpPermissions[] |"
  execCmd=$execCmd'"\(.ToPort)" + "-" + "\(.FromPort)" + " " +(.IpRanges[] |'
  execCmd=$execCmd'select(.CidrIp=="'$query'") | .CidrIp)'"'"
  myexec aws ec2 --profile $profile describe-security-groups | jq -r '.SecurityGroups[]|.GroupId' | while read row
  do 
      groupId=$row
      res=$(eval $execCmd 2> /dev/null)
      if [ -n "$res" ]; then
         echo "${groupId}:"
         echo $res
      fi;
  done
}
#awk 'BEGIN{RS="sg-"}NR > 1{for(i=2;i<=NF;i++){print "ec2SecurityAct",$i,"sg-"$1,"180.167.105.22/32","js","add","us-east-1";}}' js-sg.txt|xargs -n 1 -P 50 -i bash -c "source /vagrant/tool/src/bash/aws/ec2.sh && {}"
ec2SecurityAct() {
  port=$1
  groupId=$2
  cidr=$3
  profile=${4:-jt}
  action=${5:-add}
  region=${6:-us-east-1}
  if [  $# -eq 0 ]; then
        cat <<USAGE
  Usage: $0 [port] [groupId] [cidr] [profile] [action] [region]
USAGE
     return 
  fi;
  cmd=
  if [ "$action" = "add" ]; then
    cmd="authorize-security-group-ingress"
  elif [ "$action" = "del" ]; then
    cmd="revoke-security-group-ingress"
  fi;
  if [ -z "$cmd" ]; then
     echo input right param
     return
  fi;
  myexec aws ec2 $cmd --region $region --profile $profile --group-id $groupId --protocol tcp --port "$port" --cidr "$cidr"
}
