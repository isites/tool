#!/bin/bash

s3cp() {
    from=$1
    to=$2
    profile=${3:-jt}
    #--recursive
    if [ "$4" = "public" ]; then
        cmd="--acl=public-read"
    fi 
    myexec aws s3 --profile $profile cp $from $to $cmd
}

s3ls() {
    profile=${1:-jt}
    if [ $# -ge 2 ]; then
        file=$2
    fi
    myexec aws s3 --profile $profile ls $file
}
