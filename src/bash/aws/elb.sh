setElbCa() {
    elbName=$1
    caId=$2
    profile=${3:-jt}
    myexec aws elb set-load-balancer-listener-ssl-certificate --load-balancer-name $elbName --load-balancer-port 443 --ssl-certificate-id $caId --profile $profile
}


elbdesc() {
    elbName=$1
    profile=${2:-jt}
    myexec aws elb describe-load-balancers --profile $profile --load-balancer-names $elbName
}

elbHealthCheck() {
    elbName=$1
    profile=$2
    shift 2
    myexec aws elb describe-instance-health --profile $profile --load-balancer-name $elbName --instances $@
}


elbInstanceAct() {
    if [  $# -eq 0 ]; then
        cat <<USAGE
    Usage: $0 [elb-name] [profile] [act] [ids]
USAGE
        return 
    fi;
    elbName=$1
    profile=$2
    act=$3
    cmd=
    if [ "$act" = "add" ]; then
        cmd="register-instances-with-load-balancer"
    elif [ "$act" = "del" ]; then
        cmd="deregister-instances-from-load-balancer"
    fi;
    
    shift 3
    myexec aws elb $cmd --profile $profile --load-balancer-name $elbName --instances $@
}