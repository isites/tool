#!/bin/bash

#ps -au www-data -o "pid,lstart,cmd" --sort "start_time"

_mysql_read='mysql --default-character-set=utf8 -P$db_port -h$db_read -u$db_user -p$db_pass $db_name -Ne'
_mysql_read_title='mysql --default-character-set=utf8 -P$db_port -h$db_read -u$db_user -p$db_pass $db_name -e'
_file=${BASH_SOURCE[0]}
if [  -z "$_file" ]; then 
    _file=$0
fi;

_DIR=$(dirname $_file)
BASE_DIR=$(cd $_DIR;git rev-parse --show-toplevel)

gip() {
    ip=$1
    url=ipinfo.io
    if [ -n "$ip" ]; then
        url=ipinfo.io/$ip
    fi
    curl -s $url
}

mysql_read() {
    sql=$1
    if [ -z "$sql" ]; then
        echo "input sql"
        return
    fi
    #删除上次的变量
    db_name=
    db_port=
    #可以连接多个库
    db_name=$2
    stage=${3:-dev}
    title=$4
    source $BASE_DIR/etc/bash/$stage
    if [ -n "$title" ] && [ "$title" = "true" ]; then
        eval "$_mysql_read_title '$sql'"
    else
        eval "$_mysql_read '$sql'"
    fi;
}
