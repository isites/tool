<?php

//$to_time_zone = "America/Los_Angeles";
//$to_time_zone = "UTC";
//$from_time_zone = "Asia/Shanghai";
$from_time_zone = "America/Los_Angeles";
$to_time_zone = "Asia/Shanghai";

date_default_timezone_set($from_time_zone);
$time = strtotime("2017-10-05 1:00:00");
echo date('Y-m-d H:i:s', $time) . "\n";
date_default_timezone_set($to_time_zone);
echo date('Y-m-d H:i:s', $time) . "\n";


